import { useState, useEffect, useMemo, useCallback } from 'react';
import { Ed25519Keypair } from '@mysten/sui.js/keypairs/ed25519';
import { getFullnodeUrl, SuiClient } from '@mysten/sui.js/client';
import { generateNonce, generateRandomness } from '@mysten/zklogin';

const REDIRECT_URI =
	process.env.NODE_ENV === 'production'
		? 'https://zklogin-demo.vercel.app/auth'
		: 'http://localhost:3001/auth';

export default function useLoginURL(): [string, boolean] {
	const [nonce, setNonce] = useState<null | string>(null);
	const [loading, setLoading] = useState(false);

	const getNonce = useCallback(async () => {
		try {
			setLoading(true);
			const suiClient = new SuiClient({ url: getFullnodeUrl('devnet') });
			const { epoch } = await suiClient.getLatestSuiSystemState();

			console.log('epoch', epoch);

			const maxEpoch = Number(epoch) + 2; // this means the ephemeral key will be active for 2 epochs from now.
			const ephemeralKeyPair = new Ed25519Keypair();
			const randomness = generateRandomness();
			const calculatedNonce = generateNonce(
				ephemeralKeyPair.getPublicKey(),
				maxEpoch,
				randomness
			);

			setNonce(calculatedNonce);
		} catch (error) {
			console.error('Error getting nonce:', error);
		}
		setLoading(false);
	}, []);

	useEffect(() => {
		getNonce();
	}, [getNonce]);

	const loginURL = useMemo(() => {
		const params = new URLSearchParams({
			state: new URLSearchParams({
				redirect_uri: REDIRECT_URI,
			}).toString(),
			client_id:
				'25769832374-famecqrhe2gkebt5fvqms2263046lj96.apps.googleusercontent.com',
			redirect_uri: 'https://zklogin-dev-redirect.vercel.app/api/auth',
			response_type: 'id_token',
			scope: 'openid',
		});
		if (nonce) {
			params.append('nonce', nonce);
		}

		return `https://accounts.google.com/o/oauth2/v2/auth?${params}`;
	}, [nonce]);

	return [loginURL, loading];
}
