import { Dispatch, SetStateAction } from 'react';

export interface User {
	jwt: string;
	salt: string;
	address: string;
}

export interface AuthContextValue {
	logout: () => void;
	setJwt: Dispatch<SetStateAction<string>>;
}
