import { AuthContextValue, User } from '@/types/context';
import React, {
	ReactNode,
	createContext,
	useCallback,
	useEffect,
	useMemo,
	useState,
} from 'react';
import { jwtToAddress } from '@mysten/zklogin';
import useStoreState from 'use-store-state';
import LoginComponent from '@/components/login/login';
import { useRouter } from 'next/router';

const AuthContext = createContext<AuthContextValue | null>(null);

interface AuthProviderProps {
	children: ReactNode;
}

/**
 * Requirements for generating salt
 *
 * 1. Address Flag: `zk_login_glag = 0x05`
 * 2.
 */
const generateSalt = () => {};

const AuthProvider = ({ children }: AuthProviderProps) => {
	const router = useRouter();
	const [user, setUser] = useState<User | null>(null);
	const [jwt, setJwt] = useStoreState('jwt', '');
	const [loading, setLoading] = useState(false);

	const logout = useCallback(() => {
		setJwt('');
		setUser(null);
	}, [setJwt]);

	const reAuthenticate = useCallback(async () => {
		if (!jwt) return;
		try {
			setLoading(true);
			const response = await fetch(`/api/auth?jwt=${jwt}`);
			console.log(response);
			const result = await response.json();
			console.log(result);
			const salt = result.salt;
			if (salt) {
				const address = jwtToAddress(jwt, salt);
				setUser({
					jwt,
					salt,
					address,
				});
			}
		} catch (error) {
			console.error(error);
			// if (error === "JWK has expired") logout()
			logout();
		}
		setLoading(false);
	}, [jwt, logout]);

	useEffect(() => {
		if (router.pathname !== '/auth') reAuthenticate();
	}, [reAuthenticate, router.pathname]);

	const providerValue = useMemo<AuthContextValue>(
		() => ({ logout, setJwt }),
		[logout, setJwt]
	);

	return (
		<AuthContext.Provider value={providerValue}>
			{router.pathname === '/auth' ? (
				children
			) : loading ? (
				'Checking if you are logged in...'
			) : user ? (
				children
			) : (
				<LoginComponent />
			)}
		</AuthContext.Provider>
	);
};

export { AuthContext };
export default AuthProvider;
