import type { NextApiRequest, NextApiResponse } from 'next';

type ResponseData = {
	[key: string]: any;
};

export default async function handler(
	request: NextApiRequest,
	response: NextApiResponse<ResponseData>
) {
	const requestData = {
		jwt: request.query['jwt'],
	};

	try {
		const myHeaders1 = new Headers();
		myHeaders1.append('Content-Type', 'application/json');
		const raw1 = JSON.stringify({
			token: requestData.jwt,
		});
		const requestOptions1: RequestInit = {
			method: 'POST',
			headers: myHeaders1,
			body: raw1,
			redirect: 'follow',
		};
		const saltResponse = await fetch(
			'http://salt.api-devnet.mystenlabs.com/get_salt',
			requestOptions1
		);
		response.status(200).json(saltResponse);
		console.log('Response has been sent');
	} catch (error) {
		console.error(error);
		// @ts-ignore
		response.status(error.code || 500).send(error);
		throw new Error(String(error));
	}
}
