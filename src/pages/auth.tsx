import useAuthContext from '@/hooks/context/useAuthContext';
import { useRouter } from 'next/router';
import React, { useCallback, useEffect } from 'react';

const AuthPage = () => {
	const router = useRouter();
	const { setJwt } = useAuthContext();

	const extractJWTFromHash = useCallback(async () => {
		const hash = window.location.hash;
		const params = new URLSearchParams(hash.slice(1));
		const jwt = params.get('id_token');
		console.log('jwt', jwt);

		// if (jwt) {
		// 	setJwt(jwt);
		// 	if (router.pathname === '/auth') router.push('/');
		// } else {
		// 	router.push('/login');
		// }
	}, [router, setJwt]);

	useEffect(() => {
		extractJWTFromHash();
	}, [extractJWTFromHash]);

	return (
		<>
			<div>Extracting JWT from hash...</div>
		</>
	);
};

export default AuthPage;
