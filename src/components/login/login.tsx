import Link from 'next/link';
import styles from './login.module.css';
import useLoginURL from '@/hooks/data/useLoginURL';

export default function LoginComponent() {
	const [loginURL, loading] = useLoginURL();

	return loading ? (
		'Wait...Collating login url...'
	) : (
		<div className='flex flex-col items-center gap-4'>
			<h1 className='text-2xl text-gray-600 font-bold'>ZKLogin Demo</h1>
			<Link className={styles.link} href={loginURL}>
				<div className='max-w-[50px]' />
				Login to Google
			</Link>
		</div>
	);
}
